# CI/CD Pipeline with Explanation

This is a simplified configuration file for a CI/CD (Continuous Integration/Continuous Deployment) pipeline using GitLab CI/CD. The pipeline consists of three stages: `auth`, `build`, and `deploy`. It deploys a Flask app to an EC2 instance after building and pushing a Docker image to an ECR (Elastic Container Registry) repository.

### Stages:

1. `auth`: This stage handles AWS authentication. It assumes you are using AWS IAM roles for your CI/CD environment to securely access AWS resources.

2. `build`: In this stage, the Flask app is built into a Docker image and pushed to an ECR repository.

3. `deploy`: The final stage deploys the Docker image to an EC2 instance via SSH.

### Services:

- `docker:dind`: This service enables Docker-in-Docker (DinD) support, allowing your CI/CD jobs to run Docker commands within their own isolated containers.

### Environmental Variables:

Before explaining the pipeline steps, let's define the environmental variables that need to be set in your CI/CD environment:

1. `ROLE_ARN`: The AWS IAM role ARN that the CI/CD pipeline will assume for authentication.

2. `CI_JOB_JWT_V2`: A JSON Web Token (JWT) used to authenticate with AWS for assuming the IAM role. This token is specific to the CI/CD environment.

3. `ECR_REPOSITORY`: The name of the ECR repository where the Docker image will be pushed.

4. `CI_COMMIT_SHA`: The Git commit SHA of the current pipeline run. It will be used to tag the Docker image uniquely.

5. `SSH_PRIVATE_KEY`: The private SSH key used to access the EC2 instance.

6. `JUMP_SERVER_IP`: The IP address of the jump server (bastion host) through which you can access the EC2 instance.

7. `SSH_USER`: The SSH user that you will use to log in to the EC2 instance.

8. `PRIVATE_EC2_IP`: The private IP address of the EC2 instance where the Flask app will be deployed.

> **Note:** These environmental variables should be kept secret and managed securely in your CI/CD environment (e.g., GitLab CI/CD variables).

### Prerequisites:

Before running the CI/CD pipeline, please ensure you have completed the following steps:

1. **Add Keypair to Jump Server**:
   - Manually copy the SSH private key file (`flask-app-kp.pem`) to the jump server (bastion host) using the `scp` command:
     ```bash
     scp -i path/to/your/local/private-key.pem path/to/flask-app-kp.pem $SSH_USER@$JUMP_SERVER_IP:~/
     ```
   - Ensure that the SSH private key (`flask-app-kp.pem`) has appropriate permissions (e.g., `chmod 600 flask-app-kp.pem`).

2. **Update Database Server IP in `app.py`**:
   - Open the `app.py` file in your Flask app codebase.
   - Locate the configuration section where the Database server IP is defined.
   - Update the IP address to match the actual private IP address of your Database server.

### Pipeline Steps:

1. `auth` stage:
   - The `authenticate-aws-test` job runs in this stage.
   - It uses the Docker image `docker:stable` and installs necessary packages like `curl`, `jq`, `python3`, and `awscli`.
   - The AWS CLI is configured with the required settings using `AWS_ROLE_ARN`, `AWS_WEB_IDENTITY_TOKEN_FILE`, and `AWS_DEFAULT_REGION`.
   - The `authenticate` script is defined using YAML anchors (marked with `&authenticate`) to avoid duplication. This script writes the `CI_JOB_JWT_V2` to the `AWS_WEB_IDENTITY_TOKEN_FILE`.
   - The pipeline then verifies the AWS authentication by running `aws sts get-caller-identity`.

2. `build` stage:
   - The `build_flask_app` job runs in this stage.
   - It also uses the `docker:stable` image and sets the AWS CLI configuration.
   - The `authenticate` script is reused.
   - The pipeline then builds the Docker image for the Flask app using the Dockerfile in the project's root directory.
   - It logs in to the ECR repository using the AWS credentials and pushes the Docker image tagged with the `CI_COMMIT_SHA`.

3. `deploy` stage:
   - The `deploy_to_ec2` job runs in this stage.
   - It uses the `alpine:latest` image.
   - The job sets up SSH for accessing the EC2 instance.
   - The private SSH key is written to the `/root/.ssh/id_rsa` file, and the known hosts file is updated with the EC2 instance's IP.
   - Finally, it connects to the jump server and then SSHs into the EC2 instance.
   - On the EC2 instance, the pipeline pulls the Docker image from the ECR repository and runs the Flask app as a detached container accessible on port 80.

> **Important Notes:**
>
> 1. The pipeline will only be triggered for the `master` branch (`only: - master`). Adjust this if you want to run the pipeline on different branches or events.
>
> 2. The pipeline is designed to run on GitLab CI/CD. If you are using a different CI/CD platform, some parts of the configuration might need to be adapted.
>
> 3. Make sure to set up the correct IAM roles, policies, and permissions in AWS to allow the pipeline to perform the necessary actions.
>
> 4. Handle the environmental variables with care, as they contain sensitive information like AWS credentials and SSH keys.
>
> 5. Ensure that the EC2 instance's security groups and network settings allow access to port 80 for public access to the Flask app, if desired.
>
> 6. This is a basic pipeline; depending on your project's complexity and requirements, you may need to extend it further to include testing, linting, etc.

