FROM python:3.8

# Install pipenv
RUN pip install pipenv

# Set the working directory to /app
WORKDIR /app

# Clone the GitLab repository
RUN apt-get update && apt-get install -y git
RUN git clone https://gitlab.com/umairabid1612/typing-master.git .

# Install app dependencies using pipenv
RUN pipenv install --system --deploy --ignore-pipfile

# Expose port 5000 (assuming the Flask app listens on port 5000)
EXPOSE 80

# Run the Flask app
CMD ["python", "app.py", "&"]